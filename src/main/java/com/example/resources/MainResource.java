package com.example.resources;

import com.example.models.LeakCheckResponse;
import com.example.services.LeakCheckService;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/check")
public class MainResource {


    private final LeakCheckService service;
    public MainResource(LeakCheckService service) {
        this.service = service;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public LeakCheckResponse check(@QueryParam("target") String target,
                                   @QueryParam("type") String type) {
        return service.getDetailsByTarget(target, type);
    }

    // Phone
    // Email
    // Login
    // MineCraft ...
}