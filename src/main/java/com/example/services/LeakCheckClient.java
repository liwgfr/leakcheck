package com.example.services;

import com.example.models.LeakCheckResponse;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.QueryParam;

@RegisterRestClient(baseUri = "https://leakcheck.net/api?key=b7ca40dde251a91bdd26504a41d2fc289adbb540")
public interface LeakCheckClient {

    @GET
    LeakCheckResponse getDetailsByTarget(@QueryParam("check") String target,
                                         @QueryParam("type") String type);
}
