package com.example.services;

import com.example.models.LeakCheckResponse;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class LeakCheckService {

    @Inject
    @RestClient
    LeakCheckClient client;

    public LeakCheckResponse getDetailsByTarget(String target, String type) {
        return client.getDetailsByTarget(target, type);
    }
}
