package com.example.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeakCheckResponse implements Serializable {
    private static final long serialVersionUID = -7384200966865812591L;

    private Integer found;
    private List<LeakCheckResult> result;
}
