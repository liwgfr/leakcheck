package com.example.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LeakCheckResult implements Serializable {
    private static final long serialVersionUID = 1320459414336773254L;

    private List<String> sources;
    private String line;
    @JsonProperty("last_breach")
    private String lastBreach;
}
